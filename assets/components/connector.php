<?php
if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php')) {
    /** @noinspection PhpIncludeInspection */
    require_once dirname(dirname(dirname(dirname(__FILE__)))) . '/config.core.php';
} else {
    require_once dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config.core.php';
}
/** @noinspection PhpIncludeInspection */
require_once MODX_CORE_PATH . 'config/' . MODX_CONFIG_KEY . '.inc.php';
/** @noinspection PhpIncludeInspection */
require_once MODX_CONNECTORS_PATH . 'index.php';
/** @var adaptiveImage $adaptiveImage */
$adaptiveImage = $modx->getService('adaptiveImage', 'adaptiveImage', MODX_CORE_PATH . 'components/adaptiveimage/model/');
$modx->lexicon->load('adaptiveimage:default');

// handle request
$corePath = $modx->getOption('adaptiveimage_core_path', null, $modx->getOption('core_path') . 'components/adaptiveimage/');
$path = $modx->getOption('processorsPath', $adaptiveImage->config, $corePath . 'processors/');
$modx->getRequest();

/** @var modConnectorRequest $request */
$request = $modx->request;
$request->handleRequest([
    'processors_path' => $path,
    'location' => '',
]);