var adaptiveImage = function (config) {
    config = config || {};
    adaptiveImage.superclass.constructor.call(this, config);
};
Ext.extend(adaptiveImage, Ext.Component, {
    page: {}, window: {}, grid: {}, tree: {}, panel: {}, combo: {}, config: {}, view: {}, utils: {}
});
Ext.reg('adaptiveimage', adaptiveImage);

adaptiveImage = new adaptiveImage();