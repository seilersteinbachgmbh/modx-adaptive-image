adaptiveImage.page.Home = function (config) {
    config = config || {};
    Ext.applyIf(config, {
        components: [{
            xtype: 'adaptiveimage-panel-home',
            renderTo: 'adaptiveimage-panel-home-div'
        }]
    });
    adaptiveImage.page.Home.superclass.constructor.call(this, config);
};
Ext.extend(adaptiveImage.page.Home, MODx.Component);
Ext.reg('adaptiveimage-page-home', adaptiveImage.page.Home);