#Adaptive Image

This Modx Plugin converts img tags in HTML output to img tags with srcset and sizes (media queries) attributes and let clients download the image file in an optimized resolution.
It uses pThumb to generate resized version of the images.

##Example

The Plugin converts this code in the markup:

<img class="small-image" src="files/example.jpg" alt="example">

To this:

<img class="small-image adaptive-image" src="files/example.jpg" alt="" sizes="(min-width: 768px) 768px, 320px" srcset="/assets/components/phpthumbof/cache/example.11f7b64b1f8c27b99f221f6c6f22f935.jpg 320w, /assets/components/phpthumbof/cache/example.84e62da35b23b1a0fa4f7d0e3573a8bb.jpg 768w" width="3456" height="3456">

##Options

###adaptiveimage_image_breakpoint_widths

Comma separated list of image widths that should be generated additionally to the original uploaded image.

Example: 320,768


###adaptiveimage_image_class

This class is added to all manipulated image tags.

Example: adaptive-image


###adaptiveimage_image_container_selectors

Only image tags within this selectors in the dom document are processed.
It is possible to use class, id or Element selectors.

Example: .content,#main,article


###adaptiveimage_image_file_extensions

Only image tags with this file extension are processed.

Example: png,jpg

###adaptiveimage_image_sizes

Will be used as the screen-sizes attribute in the processed image tags. Here the window-size defines, which image should be served.

Example: (min-width: 768px) 768px, 320px
