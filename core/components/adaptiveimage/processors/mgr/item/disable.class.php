<?php

class adaptiveImageItemDisableProcessor extends modObjectProcessor
{
    public $objectType = 'adaptiveImageItem';
    public $classKey = 'adaptiveImageItem';
    public $languageTopics = ['adaptiveimage'];
    //public $permission = 'save';


    /**
     * @return array|string
     */
    public function process()
    {
        if (!$this->checkPermissions()) {
            return $this->failure($this->modx->lexicon('access_denied'));
        }

        $ids = $this->modx->fromJSON($this->getProperty('ids'));
        if (empty($ids)) {
            return $this->failure($this->modx->lexicon('adaptiveimage_item_err_ns'));
        }

        foreach ($ids as $id) {
            /** @var adaptiveImageItem $object */
            if (!$object = $this->modx->getObject($this->classKey, $id)) {
                return $this->failure($this->modx->lexicon('adaptiveimage_item_err_nf'));
            }

            $object->set('active', false);
            $object->save();
        }

        return $this->success();
    }

}

return 'adaptiveImageItemDisableProcessor';
