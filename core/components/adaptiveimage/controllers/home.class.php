<?php

/**
 * The home manager controller for adaptiveImage.
 *
 */
class adaptiveImageHomeManagerController extends modExtraManagerController
{
    /** @var adaptiveImage $adaptiveImage */
    public $adaptiveImage;


    /**
     *
     */
    public function initialize()
    {
        $this->adaptiveImage = $this->modx->getService('adaptiveImage', 'adaptiveImage', MODX_CORE_PATH . 'components/adaptiveimage/model/');
        parent::initialize();
    }


    /**
     * @return array
     */
    public function getLanguageTopics()
    {
        return ['adaptiveimage:default'];
    }


    /**
     * @return bool
     */
    public function checkPermissions()
    {
        return true;
    }


    /**
     * @return null|string
     */
    public function getPageTitle()
    {
        return $this->modx->lexicon('adaptiveimage');
    }


    /**
     * @return void
     */
    public function loadCustomCssJs()
    {
        $this->addCss($this->adaptiveImage->config['cssUrl'] . 'mgr/main.css');
        $this->addJavascript($this->adaptiveImage->config['jsUrl'] . 'mgr/adaptiveimage.js');
        $this->addJavascript($this->adaptiveImage->config['jsUrl'] . 'mgr/misc/utils.js');
        $this->addJavascript($this->adaptiveImage->config['jsUrl'] . 'mgr/misc/combo.js');
        $this->addJavascript($this->adaptiveImage->config['jsUrl'] . 'mgr/widgets/items.grid.js');
        $this->addJavascript($this->adaptiveImage->config['jsUrl'] . 'mgr/widgets/items.windows.js');
        $this->addJavascript($this->adaptiveImage->config['jsUrl'] . 'mgr/widgets/home.panel.js');
        $this->addJavascript($this->adaptiveImage->config['jsUrl'] . 'mgr/sections/home.js');

        $this->addHtml('<script type="text/javascript">
        adaptiveImage.config = ' . json_encode($this->adaptiveImage->config) . ';
        adaptiveImage.config.connector_url = "' . $this->adaptiveImage->config['connectorUrl'] . '";
        Ext.onReady(function() {MODx.load({ xtype: "adaptiveimage-page-home"});});
        </script>');
    }


    /**
     * @return string
     */
    public function getTemplateFile()
    {
        $this->content .= '<div id="adaptiveimage-panel-home-div"></div>';

        return '';
    }
}