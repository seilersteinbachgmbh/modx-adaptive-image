<?php

$_lang['area_adaptiveimage_main'] = 'Main';

$_lang['setting_adaptiveimage_container_selectors'] = 'Container selector';
$_lang['setting_adaptiveimage_container_selectors_desc'] = 'Only image tags within this containers are transformed.';

$_lang['setting_adaptiveimage_image_class'] = 'Image class';
$_lang['setting_adaptiveimage_image_class_desc'] = 'This class is added to the transformed image tags.';

$_lang['setting_adaptiveimage_image_widths'] = 'Widths of generated images';
$_lang['setting_adaptiveimage_image_widths_desc'] = 'These are the widths of the generated images in pixels, should be comma separeted array of integers.';

$_lang['setting_adaptiveimage_breakpoint_widths'] = 'Breakpoints';
$_lang['setting_adaptiveimage_breakpoint_widths_desc'] = 'These are the breakpoints for each of which a separate source is generated. Should be a comma separeted array of integers with the same length as the image wdiths.';

$_lang['setting_adaptiveimage_image_file_extensions'] = 'File extensions';
$_lang['setting_adaptiveimage_image_file_extensions_desc'] = 'File extension of image tags to be transformed.';

$_lang['setting_adaptiveimage_image_sizes'] = 'Sizes';
$_lang['setting_adaptiveimage_image_sizes_desc'] = 'Sizes attribute of generated image tag.';
