<?php
include_once 'setting.inc.php';

$_lang['adaptiveimage'] = 'adaptiveImage';
$_lang['adaptiveimage_menu_desc'] = 'A sample Extra to develop from.';
$_lang['adaptiveimage_intro_msg'] = 'You can select multiple items by holding Shift or Ctrl button.';

$_lang['adaptiveimage_items'] = 'Items';
$_lang['adaptiveimage_item_id'] = 'Id';
$_lang['adaptiveimage_item_name'] = 'Name';
$_lang['adaptiveimage_item_description'] = 'Description';
$_lang['adaptiveimage_item_active'] = 'Active';

$_lang['adaptiveimage_item_create'] = 'Create Item';
$_lang['adaptiveimage_item_update'] = 'Update Item';
$_lang['adaptiveimage_item_enable'] = 'Enable Item';
$_lang['adaptiveimage_items_enable'] = 'Enable Items';
$_lang['adaptiveimage_item_disable'] = 'Disable Item';
$_lang['adaptiveimage_items_disable'] = 'Disable Items';
$_lang['adaptiveimage_item_remove'] = 'Remove Item';
$_lang['adaptiveimage_items_remove'] = 'Remove Items';
$_lang['adaptiveimage_item_remove_confirm'] = 'Are you sure you want to remove this Item?';
$_lang['adaptiveimage_items_remove_confirm'] = 'Are you sure you want to remove this Items?';

$_lang['adaptiveimage_item_err_name'] = 'You must specify the name of Item.';
$_lang['adaptiveimage_item_err_ae'] = 'An Item already exists with that name.';
$_lang['adaptiveimage_item_err_nf'] = 'Item not found.';
$_lang['adaptiveimage_item_err_ns'] = 'Item not specified.';
$_lang['adaptiveimage_item_err_remove'] = 'An error occurred while trying to remove the Item.';
$_lang['adaptiveimage_item_err_save'] = 'An error occurred while trying to save the Item.';

$_lang['adaptiveimage_grid_search'] = 'Search';
$_lang['adaptiveimage_grid_actions'] = 'Actions';