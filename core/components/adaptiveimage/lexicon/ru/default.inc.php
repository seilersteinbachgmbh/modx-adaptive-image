<?php
include_once 'setting.inc.php';

$_lang['adaptiveimage'] = 'adaptiveImage';
$_lang['adaptiveimage_menu_desc'] = 'Пример расширения для разработки.';
$_lang['adaptiveimage_intro_msg'] = 'Вы можете выделять сразу несколько предметов при помощи Shift или Ctrl.';

$_lang['adaptiveimage_items'] = 'Предметы';
$_lang['adaptiveimage_item_id'] = 'Id';
$_lang['adaptiveimage_item_name'] = 'Название';
$_lang['adaptiveimage_item_description'] = 'Описание';
$_lang['adaptiveimage_item_active'] = 'Активно';

$_lang['adaptiveimage_item_create'] = 'Создать предмет';
$_lang['adaptiveimage_item_update'] = 'Изменить Предмет';
$_lang['adaptiveimage_item_enable'] = 'Включить Предмет';
$_lang['adaptiveimage_items_enable'] = 'Включить Предметы';
$_lang['adaptiveimage_item_disable'] = 'Отключить Предмет';
$_lang['adaptiveimage_items_disable'] = 'Отключить Предметы';
$_lang['adaptiveimage_item_remove'] = 'Удалить Предмет';
$_lang['adaptiveimage_items_remove'] = 'Удалить Предметы';
$_lang['adaptiveimage_item_remove_confirm'] = 'Вы уверены, что хотите удалить этот Предмет?';
$_lang['adaptiveimage_items_remove_confirm'] = 'Вы уверены, что хотите удалить эти Предметы?';
$_lang['adaptiveimage_item_active'] = 'Включено';

$_lang['adaptiveimage_item_err_name'] = 'Вы должны указать имя Предмета.';
$_lang['adaptiveimage_item_err_ae'] = 'Предмет с таким именем уже существует.';
$_lang['adaptiveimage_item_err_nf'] = 'Предмет не найден.';
$_lang['adaptiveimage_item_err_ns'] = 'Предмет не указан.';
$_lang['adaptiveimage_item_err_remove'] = 'Ошибка при удалении Предмета.';
$_lang['adaptiveimage_item_err_save'] = 'Ошибка при сохранении Предмета.';

$_lang['adaptiveimage_grid_search'] = 'Поиск';
$_lang['adaptiveimage_grid_actions'] = 'Действия';