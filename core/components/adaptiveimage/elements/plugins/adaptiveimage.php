<?php
function convertImageTags(&$modx, &$parentNode) {

    //settings
    $imageClass = $modx->getOption('adaptiveimage_image_class');
    $imageBreakpointWidths = explode(',', $modx->getOption('adaptiveimage_breakpoint_widths'));
    $imageFileExtensions = explode(',', $modx->getOption('adaptiveimage_image_file_extensions'));
    $imageSizes = $modx->getOption('adaptiveimage_image_sizes');
    $imageWidths = explode(',', $modx->getOption('adaptiveimage_image_widths'));

    $images = $parentNode->getElementsByTagName('img');
    $length = $images->length;

    for ($i = $length - 1; $i > -1 ; $i--) {

        $image = $images->item($i);

        $src = $image->getAttribute('src');
        $ext = strtolower(pathinfo($src, PATHINFO_EXTENSION));

        if (in_array($ext, $imageFileExtensions)) {
            $class= $image->getAttribute('class');
            $srcset = [];

            foreach ($imageBreakpointWidths as $key => $imageBreakpointWidth) {
                $imageWidth = $imageBreakpointWidth;
                if (isset($imageWidths[$key])) $imageWidth = $imageWidths[$key];
                $tmp = $modx->runSnippet('pThumb',array(
                    'input' => $src,
                    'useResizer' => '1',
                    'options' => 'w='.$imageWidth
                ));

                $srcset[] = $tmp.' '.$imageBreakpointWidth.'w';
            }
            $srcset = implode(', ', $srcset);

            $image->setAttribute('data-debug', $imageContainerSelector);
            $image->setAttribute('sizes', $imageSizes);
            $image->setAttribute('src', $src);
            $image->setAttribute('srcset', $srcset);
            $image->setAttribute('class', $class.' '.$imageClass);

        }
    }
}

/** @var modX $modx */
switch ($modx->event->name) {

    case 'OnWebPagePrerender':

        if($modx->resource->content_type != 1) return;

        $content = &$modx->resource->_output;

        $html = $modx->resource->_output;

        $dom = new DOMDocument();
        $dom->loadHTML($content);

        $imageContainerSelectors = $modx->getOption('adaptiveimage_container_selectors');
        $imageContainerSelectors = !empty($imageContainerSelectors) ? explode(',', $imageContainerSelectors) : '';

        if (empty($imageContainerSelectors)) {
            convertImageTags($modx, $dom);
        } else {
            $xpath = new DOMXPath($dom);
            foreach ($imageContainerSelectors as $selector) {
                $selector = trim($selector);

                if (substr($selector, 0, 1) === '.') {
                    $nodes = $xpath->query('//*[contains(@class, "'.substr($selector, 1).'")]');
                } else if (substr($selector, 0, 1) === '#') {
                    $nodes = $xpath->query('//*[@id, "'.substr($selector, 1).'"]');
                } else {
                    $nodes = $dom->getElementsByTagName($selector);
                }

                foreach ($nodes as $node) {
                    convertImageTags($modx, $node);
                }
            }
        }

        $html = $dom->saveHTML();

        $content = $html;

        break;
}
