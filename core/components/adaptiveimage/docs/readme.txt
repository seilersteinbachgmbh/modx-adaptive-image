--------------------
adaptiveImage
--------------------
Author: Moritz Wettstein & Tonio Seiler, Furbo GmbH (admin@furbo.ch)

--------------------
Example

The Plugin converts this code in the markup:

<img class="small-image" src="files/example.jpg" alt="example">

To this:

<img class="small-image adaptive-image" src="files/example.jpg" alt="" sizes="(min-width: 768px) 768px, 320px" srcset="/assets/components/phpthumbof/cache/example.11f7b64b1f8c27b99f221f6c6f22f935.jpg 320w, /assets/components/phpthumbof/cache/example.84e62da35b23b1a0fa4f7d0e3573a8bb.jpg 768w" width="3456" height="3456">

--------------------

A basic Extra for MODx Revolution.

--------------------
Built with
https://github.com/bezumkin/modExtra

Build Extra in browser:
http://localhost/Extras/adaptiveImage/_build/build.php

Build and download Extra in browser:
http://localhost/Extras/adaptiveImage/_build/build.php?download=1
