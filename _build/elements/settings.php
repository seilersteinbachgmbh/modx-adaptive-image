<?php

return [
    'container_selectors' => [
        'xtype' => 'textfield',
        'value' => '',
        'area' => 'adaptive-image',
    ],
    'image_class' => [
        'xtype' => 'textfield',
        'value' => 'adaptive-image',
        'area' => 'adaptive-image',
    ],
    'image_widths' => [
        'xtype' => 'textfield',
        'value' => '320,768,2000',
        'area' => 'adaptive-image',
    ],
    'breakpoint_widths' => [
        'xtype' => 'textfield',
        'value' => '320,768,1280',
        'area' => 'adaptive-image',
    ],
    'image_file_extensions' => [
        'xtype' => 'textfield',
        'value' => 'jpg,png',
        'area' => 'adaptive-image',
    ],
    'image_sizes' => [
        'xtype' => 'textfield',
        'value' => '(max-width: 320px) 320px, (max-width: 768px) 768px, 100vw',
        'area' => 'adaptive-image',
    ],
];
