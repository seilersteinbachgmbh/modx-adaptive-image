<?php

return [
    'adaptiveImage' => [
        'description' => '',
        'type' => 'file',
        'content' => '',
        'namespace' => 'adaptiveimage',
        'lexicon' => 'adaptiveimage:dashboards',
        'size' => 'half',
    ],
];