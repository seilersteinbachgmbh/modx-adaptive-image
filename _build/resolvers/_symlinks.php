<?php
/** @var xPDOTransport $transport */
/** @var array $options */
/** @var modX $modx */
if ($transport->xpdo) {
    $modx =& $transport->xpdo;

    $dev = MODX_BASE_PATH . 'Extras/adaptiveImage/';
    /** @var xPDOCacheManager $cache */
    $cache = $modx->getCacheManager();
    if (file_exists($dev) && $cache) {
        if (!is_link($dev . 'assets/components/adaptiveimage')) {
            $cache->deleteTree(
                $dev . 'assets/components/adaptiveimage/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_ASSETS_PATH . 'components/adaptiveimage/', $dev . 'assets/components/adaptiveimage');
        }
        if (!is_link($dev . 'core/components/adaptiveimage')) {
            $cache->deleteTree(
                $dev . 'core/components/adaptiveimage/',
                ['deleteTop' => true, 'skipDirs' => false, 'extensions' => []]
            );
            symlink(MODX_CORE_PATH . 'components/adaptiveimage/', $dev . 'core/components/adaptiveimage');
        }
    }
}

return true;